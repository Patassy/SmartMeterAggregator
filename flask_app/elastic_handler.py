from configuration import ELASTIC_PORT, ELASTIC_IP, RETRY_TIMEOUT
from elasticsearch import Elasticsearch


class ElasticHandler(object):
    def __init__(self):
        self._es = Elasticsearch([{'host': ELASTIC_IP, 'port': int(ELASTIC_PORT)}],
                                 retry_on_timeout=True)

    def store_record(self, index_name, type_name, record):
        '''
        stores the data in the provided data_storage index
        '''
        try:
            self._es.index(index=index_name,
                           doc_type=type_name,
                           body=record)
        except Exception as ex:
            print('Error in indexing data')