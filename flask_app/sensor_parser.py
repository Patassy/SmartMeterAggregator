from datetime import datetime
from configuration import RAW_KEY_MAP


def parse_sensor_data(raw_measurement, delim):
    '''
    Converts the raw sensor output into a dictionary

    :return: dict
    '''
    try:
        arr = raw_measurement.split(delim)
        raw = dict(zip(arr[::2], arr[1::2]))
        parsed_data = {RAW_KEY_MAP[name]: val for name, val in raw.items()}
    except (KeyError, IndexError, AttributeError):
        print('Corrupted Data')
    act_timestamp = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
    parsed_data['timestamp'] = act_timestamp
    return parsed_data