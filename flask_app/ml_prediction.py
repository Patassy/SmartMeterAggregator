import pickle
from configuration import RAW_KEY_MAP as KEY_DICT
import numpy as np
import os

PICKLE_PATH = os.path.join(os.path.dirname(__file__), 'smart_meter_svm_model.pickle')


class Predictor(object):
    def __init__(self):
        self.clf = self._get_model(PICKLE_PATH)
        self._buffer = [None, None]

    def update_meas(self, structured_values_act, structured_values_prev):
        select_columns = ['PC', 'PTA', 'H0', 'H1', 'H2', 'H3', 'H4', 'H5']
        predict_values = [int(structured_values_act[KEY_DICT[col]])
                          for col
                          in select_columns]
        act_transient = int(structured_values_act[KEY_DICT['NT']])
        prev_transient = int(structured_values_prev[KEY_DICT['NT']])
        self._buffer[0] = prev_transient
        self._buffer[1] = act_transient
        device = self._determine_device(predict_values)
        if isinstance(device, (list, tuple, np.ndarray)):
            return device[0]
        return device

    def _determine_device(self, predict_values):
        if (self._buffer[1] - self._buffer[0]) <= 0:
            return ''
        if predict_values[0] < 1:
            return ''
        prediction = self.clf.predict(np.array([predict_values]))
        return prediction

    def _get_model(self, pickle_file):
        pickle_in = open(pickle_file, 'rb')
        clf = pickle.load(pickle_in)
        return clf