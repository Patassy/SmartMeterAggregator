from flask_app.sensor_parser import parse_sensor_data


def handle_measurement_data(prev_meas, act_meas, predictor, es_object):
    '''
    Converts raw measurement into structured format and checks machine learning state

    :param prev_meas: raw string from previous smart meter measurement
    :param act_meas: raw string from actual smart meter measurement
    :return:
    '''
    structured_prev = parse_sensor_data(prev_meas, ' ')
    structured_act = parse_sensor_data(act_meas, ' ')
    device = predictor.update_meas(structured_values_act=structured_act,
                                   structured_values_prev=structured_prev)
    structured_act['device'] = device
    es_object.store_record(index_name='smartmeter', type_name='raw_data', record=structured_act)
    return str(structured_act)
