from flask import Flask
from flask import request
from flask import abort
from flask import make_response
from flask import jsonify
from flask_app.data_handler import handle_measurement_data
from flask_app.ml_prediction import Predictor
from flask_app.elastic_handler import ElasticHandler
from urllib.parse import unquote

PREDICTOR = Predictor()
ES = ElasticHandler()

app = Flask(__name__)


@app.route('/convert_sensor', methods=['GET'])
def get_conversion():
    '''handles sensor conversion
    '''
    arguments = request.args
    if len(arguments) != 2:
        abort(400)
    try:
        prev_meas = unquote(request.args.get('prev_meas'))
        act_meas = unquote(request.args.get('act_meas'))
        output = handle_measurement_data(prev_meas, act_meas, PREDICTOR, ES)
        return jsonify(output)
    except TypeError:
        abort(400)


@app.errorhandler(400)
def not_found(error):
    '''error 400 handling
    '''
    error_dict = {'error': 'Wrong parameters'}
    error_output = jsonify(error_dict)
    return make_response(error_output, 400)


if __name__ == '__main__':
    app.run(debug=True)
